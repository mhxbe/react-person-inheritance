# React Person Inheritance

A basic example to check if we can *inherit* functionality from a **Person** class.

**Spanish** & **Dutch** overwrite the `greet`-function, which is passed via props.

**English** uses the default `greet` of **Person**.

## Inheritance via ref
A more convenient way by reusing functionality is using **ref**.
Check this answer on the following Stack Overflow question: [Extending React.js Components](https://stackoverflow.com/a/25723635/3423342)