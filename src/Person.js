import React, { Component } from 'react';

const colorRoyalblue = { color: 'royalblue' };

class Person extends Component {
  greet(name) {
    return this.props.greet ? this.props.greet(name) : `Hello ${name}!`
  }

  render(){
    const { name } = this.props;

    return (
      <section>
        <h1>{this.greet(name)}</h1>

        <p style={colorRoyalblue}>{`Paragraph in <Person/> Component.`}</p>

        {this.props.children}
      </section>
    )
  }
}

export default Person;