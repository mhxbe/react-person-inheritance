import React, { Component } from 'react';
import Person from './Person';

class Spanish extends Component {
  greet = (name) => {
    return `Ola ${name}!`
  }

  render() {
    return (
      <Person
        {...this.props}
        greet={this.greet}
      >
        <p>{`${this.props.name}'s fancy paragraph in <Spanish/>.`}</p>
      </Person>
    )
  }
}

export default Spanish;