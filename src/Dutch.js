import React, { Component } from 'react';
import Person from './Person';

class Dutch extends Component {
  greet = (name) => {
    return `Hallo ${name}!`
  }

  render() {
    return (
      <Person
        {...this.props}
        greet={this.greet}
      >
        <p>{`${this.props.name}'s fancy paragraph in <Dutch/>.`}</p>
      </Person>
    )
  }
}

export default Dutch;