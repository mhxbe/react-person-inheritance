import React from 'react';
import Person from './Person';


const English = (props) => {
  const { name } = props;
  return (
    <Person {...props}>
      <p>{`${name}'s fancy paragraph in <English/>.`}</p>
    </Person>
  )
}


export default English